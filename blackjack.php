<?php
$players = array("Elena","Sandro");
$blackjack = array('A' => 11,'J' => 10,'Q' => 10,'K' => 10);
$deck = new deck;
$deck->init();
$cards = $deck->hand($players,2);
?>
<div class="page">
<?php
reset($players);
while (list($key, $name) = each($players))
{
?>
  <div id="blackjack" class="result">
  <span class="player"><?php echo $name; ?></span><br />
<?php
	reset($cards[$name]);
	while (list($key, $card) = each($cards[$name]))
	{
		$view = $deck->view($card);
?>
  <span class="card paper"><?php echo $view; ?></span>
<?php
	}
	$point[$name] = evaluation($cards,$name,$blackjack);
?>
  </div>
<?php
}
arsort($point);
?>
  <div style="clear:both;"></div>
  <div id="blackjack" class="score">
  <span class="player">Score</span><br />
<?php
reset($point);
while (list($key, $val) = each($point))
{
?>
  <span class="card"><?php echo $key." = ".$val; ?></span><br />
<?php
}
?>
  </div>
  <div id="pocker" class="start"><a href="pocker.html" class="full open">pocker</a></div>
  <div id="blackjack" class="start"><a href="blackjack.html" class="full open">blackjack</a></div>
</div>
<?php
// EVALUETION
function evaluation($cards,$player,$blackjack)
{
	$number = array();
	$point = 0;
	reset($cards[$player]);
	while (list($key, $val) = each($cards[$player]))
	{
		$card = explode("|",$val);
		array_push($number, $card[1]);
		if(is_numeric($card[1]))
		{
			$point += intval($card[1]);
		}
		else
		{
			$point += $blackjack[$card[1]];
		}
	}
	return $point;
}
?>