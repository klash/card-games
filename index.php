<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>card games</title>
<?php
include("deck.php");
$_SESSION['path'] = "/cardgames/";
?>
<link href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>css/reset.css" rel="stylesheet" type="text/css" />
<link href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>css/layout.css" rel="stylesheet" type="text/css" />
<link href="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $_SESSION['path']; ?>css/font.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php
if(isset($_GET['page']) && $_GET['page'] != "")
{
	include($_GET['page'].".php");
	include("twitter.php");
}
else
{
	include("intro.php");
}
?>
</body>
</html>