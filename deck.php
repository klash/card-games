<?php
class deck
{
	var $suit = array("spades","hearts","clubs","diams");
	var $face = array( 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", "A");
	var $deck = array();
	var $cards = array();
	function init()
	{
		reset($this->suit);
		while (list($key, $val_suit) = each($this->suit))
		{
			reset($this->face);
			while (list($key, $val_face) = each($this->face))
			{
				array_push($this->deck, $val_suit."|".$val_face);
			}
		}
	}
	function hand($players,$qty)
	{
		shuffle($this->deck);
		reset($players);
		while (list($key, $val) = each($players))
		{
			for ($i = 1; $i <= $qty; $i++)
			{
				$this->cards[$val][$i] = array_pop($this->deck);
			}
		}
		return $this->cards;
	}
	function view($card)
	{
		$change = explode("|",$card);
		$change = "&".$change[0].";<br />".$change[1];
		return $change;
	}
}
