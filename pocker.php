<?php
$players = array("Elena","Sandro");
$players = array("Michael","Marco","Ted","Luke");
$pocker = array('none' => 0,'one.pair' => 1,'two.pair' => 2,'three.kind' => 3,'straight' => 4,'flush' => 5,'full' => 6,'four.kind' => 7,'straight.flush' => 8); 
$deck = new deck;
$deck->init();
$cards = $deck->hand($players,5);
?>
<div class="page">
<?php
reset($players);
while (list($key, $name) = each($players))
{
?>
  <div id="pocker" class="result">
  <span class="player"><?php echo $name; ?></span><br />
<?php
	reset($cards[$name]);
	while (list($key, $card) = each($cards[$name]))
	{
		$view = $deck->view($card);
?>
  <span class="card paper"><?php echo $view; ?></span>
<?php
	}
	$point[$name] = evaluation($cards,$name,$deck->suit,$deck->face);
?>
  <br /><span class="player"><?php echo str_replace("."," ",$point[$name]); ?></span><br />
<?php
	$result[$name] = $pocker[$point[$name]];
?>
  </div>
<?php
}
arsort($result);
?>
  <div style="clear:both;"></div>
  <div id="pocker" class="score">
  <span class="player">Score</span><br />
<?php
reset($result);
while (list($key, $val) = each($result))
{
?>
  <span class="card"><?php echo $key." = ".$val; ?></span><br />
<?php
}
?>
  </div>
  <div id="pocker" class="start"><a href="pocker.html" class="full open">pocker</a></div>
  <div id="blackjack" class="start"><a href="blackjack.html" class="full open">blackjack</a></div>
  <div id="pocker" class="separation"></div>
</div>
<?php
// VALIDA
function evaluation($carte,$player,$seme,$valore)
{
	$colore = array();
	$numero = array();
	reset($carte[$player]);
	while (list($key, $val) = each($carte[$player]))
	{
		$carta = explode("|",$val);
		array_push($colore, $carta[0]);
		array_push($numero, $carta[1]);
	}
	$valuta = array();
	$valida_colore = valida_colore($colore,$seme);
	$valida_numero = valida_numero($numero,$valore);
	$valuta = array_merge($valuta,$valida_colore);
	$valuta = array_merge($valuta,$valida_numero);
	$risultato = "";
	if($valuta['one.pair'] == 1)
	{
		$risultato = "one.pair";
	}
	if($valuta['one.pair'] == 2)
	{
		$risultato = "two.pair";
	}
	if($valuta['three.kind'] == 1)
	{
		$risultato = "three.kind";
	}
	if($valuta['straight'] == 1)
	{
		$risultato = "straight";
	}
	if($valuta['flush'] == 1)
	{
		$risultato = "flush";
	}
	if($valuta['three.kind'] == 1 && $valuta['one.pair'] == 1)
	{
		$risultato = "full";
	}
	if($valuta['four.kind'] == 1)
	{
		$risultato = "four.kind";
	}
	if($valuta['straight'] == 1 && $valuta['flush'] == 1)
	{
		$risultato = "straight.flush";
	}
	if($risultato == "")
	{
		$risultato = "none";
	}
	return $risultato;
}

function valida_colore($colore,$seme)
{
	$valuta = array();
	reset($colore);
	while (list($key, $val) = each($colore))
	{
		$$val ++;
	}
	reset($seme);
	while (list($key, $val) = each($seme))
	{
		$risultato[$val] = intval($$val);
	}
	arsort($risultato);
	reset($risultato);
	while (list($key, $val) = each($risultato))
	{
		if($val > 0)
		{
			switch ($val)
			{
				case 5:
					$valuta['flush'] ++;
					break;
			}
		}
	}
	return($valuta);
}

function valida_numero($numero,$valore)
{
	$valuta = array();
	reset($numero);
	while (list($key, $val) = each($numero))
	{
		$$val ++;
	}
	reset($valore);
	while (list($key, $val) = each($valore))
	{
		$risultato[$val] = intval($$val);
	}
	arsort($risultato);
	reset($risultato);
	while (list($key, $val) = each($risultato))
	{
		if($val > 0)
		{
			switch ($val)
			{
				case 2:
					$valuta['one.pair'] ++;
					break;
				case 3:
					$valuta['three.kind'] ++;
					break;
				case 4:
					$valuta['four.kind'] ++;
					break;
				case 1:
					$valuta['straight'] ++;
					break;
			}
		}
	}
	if($valuta['straight'] == 5)
	{
		$conferma = verifica_scala($numero,$valore);
		if(!$conferma)
		{
			unset($valuta['straight']);
		}
	}
	else
	{
		unset($valuta['straight']);
	}
	return($valuta);
}

function verifica_scala($numero,$valore)
{
	$verifica = TRUE;
	asort($numero);
	reset($numero);
	$key = array_search($numero[0], $valore);
	for ($i = 1; $i < 5; $i++)
	{
		$key ++;
		if (!in_array($valore[$key], $numero)) {
			$verifica = FALSE;
			return $verifica;
		}
	}
	return $verifica;
}
?>
